#\ -p 4567
# frozen_string_literal: true

require './lib/http_analyzer'
run HttpAnalyzer::Application
