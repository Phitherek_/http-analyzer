# frozen_string_literal: true

module HttpAnalyzer
  class RequestLogger
    def initialize(logger:, request:)
      @logger = logger
      @request = request
    end

    def call
      logger.info '=== REQUEST START ==='
      logger.info "Method: #{request.request_method.to_s.upcase}"
      logger.info "Params: #{request.params.inspect}"
      logger.info 'Env:'
      logger.info request.env.inspect
      logger.info 'Body:'
      logger.info request.body.read.inspect
      logger.info '=== REQUEST END ==='
    end

    private

    attr_reader :logger, :request
  end
end
