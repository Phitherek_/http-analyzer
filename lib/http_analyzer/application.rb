# frozen_string_literal: true

require 'sinatra/base'
require 'sinatra/custom_logger'
require 'logger'
require_relative 'services'

module HttpAnalyzer
  class Application < Sinatra::Application
    helpers Sinatra::CustomLogger
    configure do
      log_file = File.open("#{root}/../../log/analyzer-#{environment}.log", 'a')
      log_file.sync = true
      logger = Logger.new(log_file)
      logger.level = Logger::DEBUG if development?
      set :logger, logger
    end

    get '/*' do
      HttpAnalyzer::RequestLogger.new(logger: logger, request: request).call
      status 204
    end

    post '/*' do
      HttpAnalyzer::RequestLogger.new(logger: logger, request: request).call
      status 204
    end

    put '/*' do
      HttpAnalyzer::RequestLogger.new(logger: logger, request: request).call
      status 204
    end

    patch '/*' do
      HttpAnalyzer::RequestLogger.new(logger: logger, request: request).call
      status 204
    end

    delete '/*' do
      HttpAnalyzer::RequestLogger.new(logger: logger, request: request).call
      status 204
    end

    options '/*' do
      HttpAnalyzer::RequestLogger.new(logger: logger, request: request).call
      status 204
    end
  end
end
