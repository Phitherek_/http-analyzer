# frozen_string_literal: true

require 'spec_helper'
require_relative '../../../../lib/http_analyzer/services/request_logger'

RSpec.describe HttpAnalyzer::RequestLogger, type: :service do
  describe '#call' do
    it 'properly logs request information to passed logger' do
      logger = double
      request = double
      body = double
      expect(request).to receive(:request_method).and_return(:test)
      expect(request).to receive(:params).and_return({ param1: 'value1', param2: 'value2' }.transform_keys(&:to_s))
      expect(request).to receive(:env).and_return({ VAR1: 'value1', VAR2: 'value2' }.transform_keys(&:to_s))
      expect(request).to receive(:body).and_return(body)
      expect(body).to receive(:read).and_return('request body')
      expect(logger).to receive(:info).with('=== REQUEST START ===')
      expect(logger).to receive(:info).with('Method: TEST')
      expect(logger).to receive(:info).with('Params: {"param1"=>"value1", "param2"=>"value2"}')
      expect(logger).to receive(:info).with('Env:')
      expect(logger).to receive(:info).with('{"VAR1"=>"value1", "VAR2"=>"value2"}')
      expect(logger).to receive(:info).with('Body:')
      expect(logger).to receive(:info).with('"request body"')
      expect(logger).to receive(:info).with('=== REQUEST END ===')

      described_class.new(logger: logger, request: request).call
    end
  end
end
