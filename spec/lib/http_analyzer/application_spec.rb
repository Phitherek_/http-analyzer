# frozen_string_literal: true

require 'spec_helper'
require_relative '../../../lib/http_analyzer/application'

RSpec.describe HttpAnalyzer::Application, type: :sinatra_app do
  def app
    described_class
  end

  it 'implements analyzer for GET requests' do
    request_logger = instance_double(HttpAnalyzer::RequestLogger)
    expect(HttpAnalyzer::RequestLogger).to receive(:new).and_return(request_logger)
    expect(request_logger).to receive(:call)

    get '/test'
    expect(last_response).to be_no_content
  end

  it 'implements analyzer for POST requests' do
    request_logger = instance_double(HttpAnalyzer::RequestLogger)
    expect(HttpAnalyzer::RequestLogger).to receive(:new).and_return(request_logger)
    expect(request_logger).to receive(:call)

    post '/test'
    expect(last_response).to be_no_content
  end

  it 'implements analyzer for PUT requests' do
    request_logger = instance_double(HttpAnalyzer::RequestLogger)
    expect(HttpAnalyzer::RequestLogger).to receive(:new).and_return(request_logger)
    expect(request_logger).to receive(:call)

    put '/test'
    expect(last_response).to be_no_content
  end

  it 'implements analyzer for PATCH requests' do
    request_logger = instance_double(HttpAnalyzer::RequestLogger)
    expect(HttpAnalyzer::RequestLogger).to receive(:new).and_return(request_logger)
    expect(request_logger).to receive(:call)

    patch '/test'
    expect(last_response).to be_no_content
  end

  it 'implements analyzer for DELETE requests' do
    request_logger = instance_double(HttpAnalyzer::RequestLogger)
    expect(HttpAnalyzer::RequestLogger).to receive(:new).and_return(request_logger)
    expect(request_logger).to receive(:call)

    delete '/test'
    expect(last_response).to be_no_content
  end

  it 'implements analyzer for OPTIONS requests' do
    request_logger = instance_double(HttpAnalyzer::RequestLogger)
    expect(HttpAnalyzer::RequestLogger).to receive(:new).and_return(request_logger)
    expect(request_logger).to receive(:call)

    options '/test'
    expect(last_response).to be_no_content
  end
end
