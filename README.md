# HTTP Analyzer

A simple Sinatra app that logs all request data to a log file.

## Requirements

* Ruby 3.2.2

## Setup

```shell
bundle install
```

## Run

```shell
bundle exec rackup
```

## Test

```shell
bundle exec rspec spec
```

## Lint

```shell
bundle exec rubocop --format fuubar
```

## Monitor security

```shell
bundle exec bundler-audit check --update
```

## Contribute

1. Fork repository
2. Create feature branch
3. Work on feature branch
4. Create merge request

## Enjoy!